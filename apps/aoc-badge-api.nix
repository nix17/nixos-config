{
  silver_aoc-badge-api,
  system,
  ...
}: {
  imports = [silver_aoc-badge-api.nixosModule.${system}];

  backups = ["/etc/silver_aoc-badge-api/database.db"];

  services = {
    aoc-badge-api = {
      enable = true;
      database = "/etc/silver_aoc-badge-api/database.db";
      host_port = "8060";
    };

    nginx.virtualHosts."api.brendan.ie" = {
      forceSSL = true;
      useACMEHost = "brendan";
      locations."/aoc".proxyPass = "http://localhost:8060";
    };
  };
}
