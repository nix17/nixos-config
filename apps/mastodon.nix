{
  config,
  lib,
  ...
}: let
  cfg = config.services.mastodon;
  hostname = "mastodon.brendan.ie";
  email = "mastodon@brendan.ie";
in {
  imports = [
  ];

  options = {};

  config = {
    backups = [
      cfg.otpSecretFile
      cfg.secretKeyBaseFile
      cfg.vapidPrivateKeyFile
      cfg.vapidPublicKeyFile
    ];

    # for lemmy?
    #nixpkgs.config.permittedInsecurePackages = [
    #  "nodejs-14.21.3"
    #  "openssl-1.1.1u"
    #];

    age.secrets.email_mastodon_pw.file = ../secrets/email_mastodon_pw.age;

    services = {
      mastodon = {
        enable = true;
        localDomain = hostname;
        configureNginx = true;

        enableUnixSocket = true;
        smtp = {
          createLocally = false;
          fromAddress = email;
          host = "mail.brendan.ie";
          user = email;
          passwordFile = config.age.secrets.email_mastodon_pw.path;
        };
        streamingProcesses = 3;
        extraConfig.SINGLE_USER_MODE = "true";
      };

      nginx.virtualHosts."${hostname}" = {
        useACMEHost = lib.mkForce "brendan";
        enableACME = lib.mkForce false;
      };
    };
  };
}
