{
  silver_better300,
  system,
  ...
}: {
  imports = [silver_better300.nixosModule.${system}];

  backups = ["/etc/silver_better300/database.db"];

  services = {
    better300 = {
      enable = true;
      database = "/etc/silver_better300/database.db";
      host_port = "127.0.0.1:8061";
    };

    nginx.virtualHosts."api.brendan.ie" = {
      forceSSL = true;
      useACMEHost = "brendan";
      locations."/bus" = {
        proxyPass = "http://localhost:8061";
        extraConfig = "add_header Access-Control-Allow-Origin *;";
      };
    };
  };
}
