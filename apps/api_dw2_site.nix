{
  silver_api_dw2_site,
  system,
  pkgs,
  ...
}: let
  name = "api_dw2_site";
in {
  imports = [silver_api_dw2_site.nixosModule.${system}];

  age.secrets."${name}".file = ../secrets/${name}.age;

  services = {
    "${name}" = {
      enable = true;
      config = "/run/agenix/${name}";
    };

    nginx.virtualHosts."api.datawars2.ie" = {
      forceSSL = true;
      useACMEHost = "datawars";
      locations."/gw2_site".proxyPass = "http://localhost:8098";
    };
  };
}
