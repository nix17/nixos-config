{
  silver_datawars2-frontend,
  system,
  ...
}: let
  base = {
    forceSSL = true;
    root = "${silver_datawars2-frontend.defaultPackage."${system}"}";
  };
in {
  services = {
    nginx.virtualHosts = {
      "datawars2.ie" = base // {useACMEHost = "datawars";};
      "www.datawars2.ie" = base // {useACMEHost = "datawars";};
    };
  };
}
