{
  lib,
  config,
  ...
}: {
  # using https://github.com/greaka/ops/blob/818be4c4dea9129abe0f086d738df4cb0bb38288/apps/restic/options.nix as a base
  options = {
    backups = lib.mkOption {
      default = [];
      type = lib.types.listOf lib.types.str;
      description = ''
        A list of paths to backup.
      '';
    };
  };

  config = {
    age.secrets.restic.file = ../secrets/restic.age;
    age.secrets.backblaze.file = ../secrets/backblaze.age;

    services.restic.backups.backblaze = {
      repository = "b2:NixOS-Main2:/backups/backblaze";
      paths = config.backups;
      initialize = true;
      passwordFile = "/run/agenix/restic";
      environmentFile = "/run/agenix/backblaze";

      # keep alst two months of snapshots
      pruneOpts = [
        #"--keep-within 0y2m0d0h"
        "--keep-monthly 2"
      ];
    };
  };
}
