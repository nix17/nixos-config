{
  compsoc_qr,
  system,
  ...
}: let
  base = {
    forceSSL = true;
    root = "${compsoc_qr.defaultPackage."${system}"}";

    # https://stackoverflow.com/a/38238001/11964934
    extraConfig = ''
      location / {
        if ($request_uri ~ ^/(.*)\.html) {
          return 302 /$1;
        }
        try_files $uri $uri.html $uri/ =404;
      }
    '';
  };
in {
  services = {
    nginx.virtualHosts = {
      "qr.brendan.ie" = base // {useACMEHost = "brendan";};
    };
  };
}
