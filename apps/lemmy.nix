{
  lib,
  config,
  pkgs,
  nixpkgs,
  ...
}: let
  hostname = "lemmy.brendan.ie";
  backend = "http://127.0.0.1:${toString config.services.lemmy.settings.port}";
  frontend = "http://127.0.0.1:${toString config.services.lemmy.ui.port}";
in {
  imports = [
    #./lemmy_custom.nix
  ];

  options = {};

  config = {
    # for lemmy?
    nixpkgs.config.permittedInsecurePackages = [
      "nodejs-14.21.3"
      "openssl-1.1.1u"
    ];

    services = {
      lemmy = {
        enable = true;
        settings = {
          hostname = hostname;
        };

        database.createLocally = true;
      };

      nginx.virtualHosts."${hostname}" = {
        forceSSL = true;
        useACMEHost = "brendan";
        locations = {
          "~ ^/(api|pictrs|feeds|nodeinfo)" = {
            proxyPass = backend;
            proxyWebsockets = true;
            recommendedProxySettings = true;
          };
          "/" = {
            # mixed frontend and backend requests, based on the request headers
            proxyPass = "$proxpass";
            recommendedProxySettings = true;
            extraConfig = ''
              set $proxpass "${frontend}";
              if ($http_accept = "application/activity+json") {
                set $proxpass "${backend}";
              }
              if ($http_accept = "application/ld+json; profile=\"https://www.w3.org/ns/activitystreams\"") {
                set $proxpass "${backend}";
              }
              if ($request_method = POST) {
                set $proxpass "${backend}";
              }

              # Cuts off the trailing slash on URLs to make them valid
              rewrite ^(.+)/+$ $1 permanent;
            '';
          };
        };
      };
    };
  };
}
