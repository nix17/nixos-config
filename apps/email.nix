{
  simple-nixos-mailserver,
  config,
  ...
}: {
  imports = [
    simple-nixos-mailserver.nixosModule
    ./acme.nix
    ./nginx.nix
  ];

  backups = [
    "/var/vmail"
    "/var/dkim"
  ];

  # passwords
  age.secrets.email_brendan.file = ../secrets/email_brendan.age;
  age.secrets.email_service.file = ../secrets/email_monica.age;
  age.secrets.email_support.file = ../secrets/email_support.age;
  age.secrets.email_lemmy.file = ../secrets/email_lemmy.age;
  age.secrets.email_mastodon.file = ../secrets/email_mastodon.age;

  services.nginx.virtualHosts."mail.brendan.ie" = {
    forceSSL = true;
    useACMEHost = "brendan";
  };

  mailserver = {
    enable = true;
    fqdn = "mail.brendan.ie";
    domains = [
      "brendan.ie"
      "brendn.ie"
      "xn--brendn-tta.ie"
      "datawars2.ie"
    ];

    # use the letsencrypt certs
    certificateScheme = "acme";

    # 100MB max size
    messageSizeLimit = 100000000;

    # tired of only having it on one thunderbird instance
    enableManageSieve = true;

    # A list of all login accounts. To create the password hashes, use
    # nix-shell -p apacheHttpd
    # htpasswd -nbB "" "password" | cut -d: -f2
    loginAccounts = {
      # main account
      "brendan@brendan.ie" = {
        hashedPasswordFile = "/run/agenix/email_brendan";

        aliases = [
          "@brendan.ie"
          "@brendn.ie"
          "@xn--brendn-tta.ie"
        ];
      };

      "service@brendan.ie" = {
        hashedPasswordFile = config.age.secrets.email_service.path;
        aliases = [
          "monica@brendan.ie"
          "vaultwarden@brendan.ie"
        ];
      };

      "support@datawars2.ie" = {
        hashedPasswordFile = "/run/agenix/email_support";
        aliases = [
          "recovery@datawars2.ie"
          "gemstore_notifier@datawars2.ie"
        ];
      };

      "lemmy@brendan.ie" = {
        hashedPasswordFile = config.age.secrets.email_lemmy.path;
      };

      "mastodon@brendan.ie" = {
        hashedPasswordFile = config.age.secrets.email_mastodon.path;
      };
    };

    # feckin spammers
    rejectRecipients = [
      "ttest@brendan.ie"
      "test1234@brendan.ie"
      "confirmation@brendan.ie"
      "accounts@brendan.ie"
      "finance@brendan.ie"
      "reilly@brendan.ie"
      # a honeypot address for folks scraping my website
      "contact@brendan.ie"
    ];
  };

  # tune the spam filter
  services.rspamd.extraConfig = ''
    actions {
      reject = null; # Disable rejects, default is 15
      add_header = 7; # Add header when reaching this score
      greylist = 4; # Apply greylisting when reaching this score
    }
  '';
}
