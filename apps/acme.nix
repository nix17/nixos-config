{
  # group that will own the certificates
  users.groups.acme = {};

  age.secrets.acme.file = ../secrets/acme.age;

  security.acme = {
    defaults = {
      email = "acme@brendan.ie";
      dnsProvider = "cloudflare";
      credentialsFile = "/run/agenix/acme";
    };

    preliminarySelfsigned = false;
    acceptTerms = true;

    certs = {
      "brendan" = {
        domain = "brendan.ie";
        extraDomainNames = [
          "*.brendan.ie"
          #"mail.brendan.ie:587"
        ];
      };

      "brendn" = {
        domain = "brendn.ie";
        extraDomainNames = ["*.brendn.ie"];
      };

      "brendan_fada" = {
        domain = "xn--brendn-tta.ie";
        extraDomainNames = ["*.xn--brendn-tta.ie"];
      };

      "datawars" = {
        domain = "datawars2.ie";
        extraDomainNames = ["*.datawars2.ie"];
      };

      "fo76" = {
        domain = "fo76.ie";
        extraDomainNames = ["*.fo76.ie"];
      };

      "silveress" = {
        domain = "silveress.ie";
        extraDomainNames = ["*.silveress.ie"];
      };

      "coffee" = {
        domain = "brendan.coffee";
        extraDomainNames = ["*.brendan.coffee"];
      };

      "growlr" = {
        domain = "growlr.ie";
        extraDomainNames = ["*.growlr.ie"];
      };

      # ᚁᚏᚓᚅᚇᚐᚅ.com (ogham for brendan)
      "brendan_ogham" = {
        domain = "xn--7ueiah2bis.com";
        extraDomainNames = ["*.xn--7ueiah2bis.com"];
      };

      "conor_a1" = {
        domain = "hi-conor-ryan-give-me-a1.ie";
        extraDomainNames = ["*.hi-conor-ryan-give-me-a1.ie"];
      };

      "conor_a1_2" = {
        domain = "hi-conor-ryan-please-give-us-an-a1.ie";
        extraDomainNames = ["*.hi-conor-ryan-please-give-us-an-a1.ie"];
      };
    };
  };
}
