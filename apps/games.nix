{arion, ...}: {
  imports = [arion.nixosModules.arion];

  backups = [
    "/etc/silver_satisfactory/config/"
    "/etc/silver_valheim/config/"
  ];

  age.secrets.valheim.file = ../secrets/valheim.age;

  virtualisation.arion = {
    backend = "podman-socket";
    projects = {
      valheim.settings = {
        #docker-compose.raw.networks.default.name = "valheim";

        services.valheim = {
          service.image = "lloesche/valheim-server";
          service.environment.env_file = "/run/agenix/valheim";
          service.volumes = [
            "/etc/silver_valheim/config:/config"
            "/etc/silver_valheim/data:/opt/valheim"
          ];
          service.ports = [
            "2456-2457:2456-2457/udp"
            "9001:9001/tcp"
          ];
        };
      };

      satisfactory.settings = {
        #docker-compose.raw.networks.default.name = "satisfactory";

        services.satisfactory = {
          service.image = "wolveix/satisfactory-server:latest";
          # setting these here as they arent special
          service.environment = {
            MAXPLAYERS = 4;
            PGID = 1000;
            PUID = 1000;
            STEAMBETA = "false";
          };

          service.volumes = [
            "/etc/silver_satisfactory/config:/config"
            "/etc/silver_satisfactory/gamefiles:/config/gamefiles"
          ];
          service.ports = [
            "7777:7777/udp"
            "15000:15000/udp"
            "15777:15777/udp"
          ];
        };
      };
    };
  };

  services = {
    nginx.virtualHosts = {
      "valhiem.brendan.ie" = {
        forceSSL = true;
        useACMEHost = "brendan";

        locations."/".proxyPass = "http://localhost:2456";
      };
    };
  };
}
