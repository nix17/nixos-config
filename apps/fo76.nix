{
  silver_fo76,
  system,
  ...
}: let
  name = "fo76";

  base = {
    forceSSL = true;
    root = "${silver_fo76.packages."${system}".frontend}";
  };
in {
  imports = [silver_fo76.nixosModule.${system}];

  age.secrets."${name}".file = ../secrets/${name}.age;

  services = {
    api_fo76 = {
      enable = true;
      config = "/run/agenix/${name}";
    };

    nginx.virtualHosts = {
      "fo76.ie" = base // {useACMEHost = "fo76";};
      "www.fo76.ie" = base // {useACMEHost = "fo76";};

      "api.fo76.ie" = {
        forceSSL = true;
        useACMEHost = "fo76";
        locations."/".proxyPass = "http://localhost:8089";
      };
    };
  };
}
