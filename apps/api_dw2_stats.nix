{
  silver_api_dw2_stats,
  system,
  pkgs,
  ...
}: let
  name = "api_dw2_stats";
in {
  imports = [silver_api_dw2_stats.nixosModule.${system}];

  age.secrets."${name}".file = ../secrets/${name}.age;

  services = {
    "${name}" = {
      enable = true;
      config = "/run/agenix/${name}";
    };

    nginx.virtualHosts."api.datawars2.ie" = {
      forceSSL = true;
      useACMEHost = "datawars";
      locations."/gw2_stats".proxyPass = "http://localhost:8086";
    };
  };
}
