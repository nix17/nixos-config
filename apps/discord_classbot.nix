{
  silver_discord_classbot,
  system,
  ...
}: let
  name = "lm121_classbot";
in {
  imports = [silver_discord_classbot.nixosModule.${system}];

  backups = ["/etc/silver_lm121_classbot/database.db"];

  age.secrets."${name}".file = ../secrets/${name}.age;

  services = {
    lm121_classbot = {
      enable = true;
      config = "/run/agenix/${name}";
    };
  };
}
