{
  silver_api_bns,
  system,
  pkgs,
  ...
}: let
  name = "api_bns";
in {
  imports = [silver_api_bns.nixosModule.${system}];

  age.secrets."${name}".file = ../secrets/${name}.age;

  services = {
    "${name}" = {
      enable = true;
      config = "/run/agenix/${name}";
    };

    nginx.virtualHosts."api.silveress.ie" = {
      forceSSL = true;
      useACMEHost = "silveress";
      locations."/bns".proxyPass = "http://localhost:8084";
    };
  };
}
