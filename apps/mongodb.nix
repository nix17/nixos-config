{
  pkgs,
  lib,
  ...
}: let
  rootDir = "/etc/silver_mongod";
  base = "/run/current-system/sw/bin";
in {
  # for backups
  backups = ["/etc/silver_mongod/backup/"];

  age.secrets.mongodb.file = ../secrets/mongodb.age;
  age.secrets.mongodb_backup.file = ../secrets/mongodb_backup.age;

  nixpkgs.config = {
    allowUnfreePredicate = pkg:
      builtins.elem (lib.getName pkg) [
        "mongodb"
      ];
  };

  services = {
    mongodb = {
      enable = true;

      package = pkgs.mongodb-6_0;

      dbpath = "${rootDir}/db";
      bind_ip = "127.0.0.1,db1.datawars2.ie";
      enableAuth = true;
      initialRootPassword = "/run/agenix/mongodb";
    };
  };

  networking.firewall.allowedTCPPorts = [27017];

  environment.systemPackages = [pkgs.mongodb-tools];

  systemd = {
    services.silver_mongod_backup = {
      description = "Mongod backup";

      wantedBy = [];
      after = ["network-online.target"];
      serviceConfig = {
        Type = "oneshot";
        User = "root";
        # slightly bastardised but it works
        ExecStart = [
          "${base}/find ${rootDir}/backup/ -mindepth 1 -mtime +7 -delete"
          "${base}/bash -c '${base}/mongodump --username $DB_USER --password $DB_PASS --oplog --archive=${rootDir}/backup/$(${base}/date +%%F).gz --gzip'"
        ];
        EnvironmentFile = "/run/agenix/mongodb_backup";
      };
    };

    timers.silver_mongod_backup = {
      description = "Run Mongod backup";

      wantedBy = ["timers.target"];
      partOf = ["silver_mongod_backup.service"];
      timerConfig = {
        OnCalendar = "23:00:00";
        Unit = "silver_mongod_backup.service";
        Persistent = true;
      };
    };
  };
}
