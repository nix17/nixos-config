# using K900's one https://gitlab.com/K900/nix/-/blob/a69502b8bf39fd99a85342b2f7989fe5896a6ae0/applications/base/nginx.nix
{pkgs, ...}: {
  services.nginx = {
    enable = true;
    package = pkgs.nginxMainline;

    recommendedOptimisation = true;
    recommendedTlsSettings = true;
    recommendedGzipSettings = true;
    recommendedProxySettings = true;

    statusPage = true;

    # give Nginx access to our certs
    group = "acme";
  };

  networking.firewall.allowedTCPPorts = [80 443];
}
