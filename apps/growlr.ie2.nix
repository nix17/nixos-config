{growlr2, ...}: {
  imports = [growlr2.nixosModules.default];

  services = {
    # enable the package
    growlr2.enable = true;

    nginx.virtualHosts = {
      "cs4227.growlr.ie" = {
        forceSSL = true;
        useACMEHost = "growlr";
        locations."/".proxyPass = "http://localhost:8091";
      };
    };
  };
}
