{
  config,
  lib,
  pkgs,
  ...
}: {
  # shove the entire config file into secrets
  age.secrets.ulfm.file = ../secrets/ulfm.age;

  ###### implementation
  networking.firewall.allowedTCPPorts = [8000];

  users.groups."icecast" = {};

  users.users."icecast2" = {
    createHome = true;
    isSystemUser = true;
    home = "/etc/icecast2";
    group = "icecast";
  };

  systemd.services.icecast = {
    after = ["network.target"];
    description = "Icecast Network Audio Streaming Server";
    wantedBy = ["multi-user.target"];

    preStart = "mkdir -p /var/log/icecast && chown nobody:nogroup /var/log/icecast";
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.icecast}/bin/icecast -c /run/agenix/ulfm";
      ExecReload = "${pkgs.coreutils}/bin/kill -HUP $MAINPID";
    };
  };

  services.nginx.virtualHosts."stream.ulcompsoc.ie" = {
    forceSSL = true;
    useACMEHost = "ulcompsoc.ie";
    locations."/".proxyPass = "http://localhost:8000";
  };
}
