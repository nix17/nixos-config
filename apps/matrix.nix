{
  config,
  lib,
  pkgs,
  ...
}: let
  hostname = "matrix.brendan.ie";
  email = "matrix@brendan.ie";
  pkg =
    (import (pkgs.fetchFromGitLab {
      owner = "famedly";
      repo = "conduit";
      rev = "b11855e7a1fc00074a13f9d1b9ab04462931332f";
      sha256 = "sha256-hqjRGQIBmiWpQPhvix8L5rcxeuJ2z0KZS6A6RbmTB/o=";
    }))
    .outputs
    .packages
    .x86_64-linux
    .default;

  baseUrl = "https://${hostname}";
  clientConfig."m.homeserver".base_url = baseUrl;
  serverConfig."m.server" = "${hostname}:443";
  mkWellKnown = data: ''
    default_type application/json;
    add_header Access-Control-Allow-Origin *;
    return 200 '${builtins.toJSON data}';
  '';
in {
  imports = [
  ];

  options = {};

  config = {
    backups = [
      config.services.matrix-conduit.settings.global.database_path
    ];

    services = {
      matrix-conduit = {
        enable = true;
        package = pkg;
        settings.global = {
          server_name = hostname;
          enable_lightning_bolt = false;
          database_backend = "rocksdb";
          allow_registration = false;
        };
      };

      # from https://github.com/greaka/ops/blob/7bf240c4862ea9d176017f79fdfc1dbe28d44825/apps/matrix/default.nix
      nginx.virtualHosts = {
        "${hostname}" = {
          forceSSL = true;
          useACMEHost = "brendan";
          listen = with lib;
            flatten (map (addr:
              map (port: {
                inherit addr port;
                ssl = port != 80;
              }) [80 443 8448])
            config.services.nginx.defaultListenAddresses);

          root = pkgs.element-web.override {
            conf = {
              default_server_config = clientConfig;
            };
          };
          locations = {
            "/_matrix" = {
              proxyPass = "http://localhost:${
                toString config.services.matrix-conduit.settings.global.port
              }";
              proxyWebsockets = true;
              extraConfig = ''
                proxy_set_header Host $host;
                proxy_buffering off;
              '';

              priority = 200;
            };

            "= /.well-known/matrix/server".extraConfig = mkWellKnown serverConfig;
            "= /.well-known/matrix/client".extraConfig = mkWellKnown clientConfig;
          };
        };
      };
    };
  };
}
