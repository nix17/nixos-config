{
  silver_brendan-ie,
  system,
  ...
}: let
  base = {
    forceSSL = true;
    root = "${silver_brendan-ie.defaultPackage."${system}"}";

    # https://stackoverflow.com/a/38238001/11964934
    extraConfig = ''
      location / {
        if ($request_uri ~ ^/(.*)\.html) {
          return 302 /$1;
        }
        try_files $uri $uri.html $uri/ =404;
      }
    '';
  };
in {
  services = {
    nginx.virtualHosts = {
      # pwersonal website
      "brendan.ie" = base // {useACMEHost = "brendan";};
      "brendn.ie" = base // {useACMEHost = "brendn";};
      "xn--brendn-tta.ie" = base // {useACMEHost = "brendan_fada";};
      "brendan.coffee" = base // {useACMEHost = "coffee";};
      "xn--7ueiah2bis.com" = base // {useACMEHost = "brendan_ogham";};

      # for hosting some basic files
      "files.brendan.ie" = {
        forceSSL = true;
        useACMEHost = "brendan";
        root = "/etc/silver_files";
      };
    };
  };
}
