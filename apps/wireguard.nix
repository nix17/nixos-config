let
  prefix = "10.13.13";
in {
  age.secrets.wireguard.file = ../secrets/wireguard.age;

  networking = {
    nat = {
      enable = true;
      internalInterfaces = ["wg0"];
    };

    firewall = {
      allowedUDPPorts = [51820];
      interfaces.wg0 = {
        allowedTCPPorts = [53];
        allowedUDPPorts = [53];
      };
    };

    wireguard.interfaces.wg0 = {
      ips = ["${prefix}.0/16"];
      listenPort = 51820;
      privateKeyFile = "/run/agenix/wireguard";

      peers = [
        {
          # Personal
          publicKey = "r1k/eA7pwYJhg547QrA4i2PnjpBqd2eyvu97e7oEeGw=";
          allowedIPs = ["${prefix}.2/32"];
        }
        {
          # Personal
          publicKey = "KwmEBlrkTWmmIY/b4oRAQlikBOaG61q21jIz+qqScxU=";
          allowedIPs = ["${prefix}.3/32"];
        }
        {
          # Incognito
          publicKey = "gVLx9Qooo70wnyBBW1NLYvNKtrI7+by9K5OrYihtsy0=";
          allowedIPs = ["${prefix}.8/32"];
        }
        {
          # Incognito
          publicKey = "6LB02kvuG+ld8L1RfNK8QQ1ftCs05MyexAIJzhUygRk=";
          allowedIPs = ["${prefix}.9/32"];
        }
      ];
    };
  };
}
