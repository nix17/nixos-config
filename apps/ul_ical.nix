{
  silver_ul_ical,
  system,
  ...
}: {
  imports = [silver_ul_ical.nixosModule.${system}];

  backups = ["/etc/silver_ul_ical/database.db"];

  services = {
    ul_ical = {
      enable = true;
      port = "8062";
      database = "/etc/silver_ul_ical/database.db";
    };

    nginx.virtualHosts."api.brendan.ie" = {
      forceSSL = true;
      useACMEHost = "brendan";
      locations."/timetable" = {
        proxyPass = "http://localhost:8062";
        extraConfig = "add_header Access-Control-Allow-Origin *;";
      };
    };
  };
}
