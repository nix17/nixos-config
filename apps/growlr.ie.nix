{growlr, ...}: {
  imports = [growlr.nixosModules.default];

  services = {
    # enable the package
    growlr.enable = true;

    nginx.virtualHosts = {
      "growlr.ie" = {
        forceSSL = true;
        useACMEHost = "growlr";
        locations."/".proxyPass = "http://localhost:8090";
      };
      "hi-conor-ryan-give-me-a1.ie" = {
        forceSSL = true;
        useACMEHost = "conor_a1";
        locations."/".proxyPass = "http://localhost:8090";
      };
      "hi-conor-ryan-please-give-us-an-a1.ie" = {
        forceSSL = true;
        useACMEHost = "conor_a1_2";
        locations."/".proxyPass = "http://localhost:8090";
      };
    };
  };
}
