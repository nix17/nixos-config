{
  silver_proxy-rate-limiter,
  system,
  ...
}: let
  name = "proxy-rate-limiter";
in {
  imports = [silver_proxy-rate-limiter.nixosModule.${system}];

  age.secrets."${name}".file = ../secrets/${name}.age;

  services = {
    proxy-rate-limiter = {
      enable = true;
      config = "/etc/silver_${name}/data.json";
      #host_port = "127.0.0.1:8060";
    };
  };

  environment.etc."${name}" = {
    source = "/run/agenix/${name}";
    # relative to /etc/
    target = "silver_${name}/data.json";
    user = "${name}";
    group = "${name}";
    mode = "0400";
  };
}
