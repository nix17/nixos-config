{
  silver_api_dw2,
  system,
  pkgs,
  ...
}: let
  name = "api_dw2";
in {
  imports = [silver_api_dw2.nixosModule.${system}];

  age.secrets."${name}".file = ../secrets/${name}.age;

  services = {
    "${name}" = {
      enable = true;
      config = "/run/agenix/${name}";
    };

    nginx.virtualHosts."api.datawars2.ie" = {
      forceSSL = true;
      useACMEHost = "datawars";
      locations."/gw2".proxyPass = "http://localhost:8085";
    };
  };
}
