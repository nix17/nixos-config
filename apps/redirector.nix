{
  silver_redirector,
  system,
  ...
}: {
  imports = [silver_redirector.nixosModule.${system}];

  age.secrets.redirector.file = ../secrets/redirector.age;

  services = {
    redirector = {
      enable = true;
      config_file = "/etc/silver_redirector/config.toml";
    };

    nginx.virtualHosts."hello.brendan.ie" = {
      forceSSL = true;
      useACMEHost = "brendan";
      locations."/".proxyPass = "http://localhost:8081";
    };
  };

  # Yoinked from Greaka
  environment.etc.redirector = {
    source = "/run/agenix/redirector";
    # relative to /etc/
    target = "silver_redirector/config.toml";
    user = "redirector";
    group = "redirector";
    mode = "0400";
  };
}
