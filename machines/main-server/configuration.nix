{
  config,
  nixpkgs,
  pkgs,
  home-manager,
  arion,
  agenix,
  system,
  ...
}: {
  imports = [
    # modules
    home-manager.nixosModule

    # system config
    ./hardware-configuration.nix

    # apps
    ../../apps/redirector.nix

    ../../apps/aoc-badge-api.nix
    ../../apps/brendan.ie.nix
    ../../apps/proxy-rate-limiter.nix
    ../../apps/datawars2-frontend.nix
    ../../apps/mongodb.nix
    ../../apps/api_bns.nix
    ../../apps/api_dw2_stats.nix
    ../../apps/api_dw2.nix
    ../../apps/api_dw2_site.nix
    #../../apps/discord_foxy.nix
    ../../apps/fo76.nix

    # docker/podman stuff
    ../../apps/games.nix

    # services
    ../../apps/wireguard.nix
    ../../apps/acme.nix
    ../../apps/nginx.nix
    ../../apps/restic.nix
    ../../apps/email.nix
    ../../apps/lemmy.nix
    ../../apps/mastodon.nix
    ../../apps/matrix.nix

    # uni
    ../../apps/better300.nix
    ../../apps/discord_classbot.nix
    ../../apps/ul_ical.nix
    ../../apps/compsoc_qr.nix

    # meme
    ../../apps/growlr.ie.nix
    ../../apps/growlr.ie2.nix
  ];

  # system specific config
  boot.loader.grub = {
    enable = true;
    device = "/dev/nvme0n1";
  };

  system.stateVersion = "22.05";
  networking.hostName = "main-server";

  # ssh
  services.openssh = {
    enable = true;
    settings.PermitRootLogin = "prohibit-password";
  };

  # flakes
  nix = {
    package = pkgs.nixFlakes; # or versioned attributes like nixVersions.nix_2_8
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  # setup for root
  users.users.root = {
    initialHashedPassword = "";

    openssh.authorizedKeys.keys = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINQHZQA1ovXw4DWDUD7ivh4eW0lnCsHVvSvBCrlAWN/b Hetzner NixOS Tester"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILCxYsyaUAV5x0x2enRs/MaI5kgBID3HiWI3g9CGRax9 Gitlab To Server"
    ];
  };

  networking.nameservers = [
    # Cloudflare
    "1.1.1.1"
    # Google
    "8.8.8.8"
    # Quad9
    "9.9.9.9"
  ];

  environment.etc = {
    "resolv.conf".text = ''
      domain your-server.de
         nameserver 1.1.1.1
      nameserver 8.8.8.8
      nameserver 9.9.9.9
         options edns0
    '';
  };

  home-manager.users.root = {
    home.packages = [
      pkgs.nodejs
      pkgs.yarn
      pkgs.htop
      pkgs.ncdu_2
      pkgs.nmap
      pkgs.bind
      agenix.packages."${system}".default
    ];

    home.stateVersion = "20.09";
    programs.home-manager.enable = true;

    programs.git = {
      enable = true;
      userName = "Brendan Golden";
      userEmail = "git_server@brendan.ie";
      extraConfig = {
        init.defaultBranch = "main";
      };
      lfs.enable = true;
    };
  };
}
