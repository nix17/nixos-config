# agenix -e name.age
let
  # /root/.ssh
  root = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN5HoUeR6ho5QycoxfRxdPw+/XH8nrpQYwgxRP0+q1zM";
  desktop = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN34yTh0nk7HAz8id5Z/wiIX3H7ptleDyXy5bfbemico Desktop";
  desktop_wsl = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBHbsOjxIcLasz+CHA8gUg1pvc8dPHwMKdWoIwNvPxLp Desktop_WSL";
  laptop_wsl = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEHNLroAjCVR9Tx382cqdxPZ5KY32r/yoQH1mgsYNqpm Silver_Laptop_WSL_Deb";

  users = [
    root
    desktop
    desktop_wsl
    laptop_wsl
  ];

  # /etc/ssh
  system1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFXnf8SBPk5+2HSw/nmK7rQ6amAQATx50NNLTmm0EHTk";
  systems = [system1];
in {
  #"secret1.age".publicKeys = [ user1 system1 ];
  #"secret2.age".publicKeys = users ++ systems;
  "redirector.age".publicKeys = users ++ systems;
  "proxy-rate-limiter.age".publicKeys = users ++ systems;
  "lm121_classbot.age".publicKeys = users ++ systems;

  "api_bns.age".publicKeys = users ++ systems;
  "api_dw2_stats.age".publicKeys = users ++ systems;
  "api_dw2.age".publicKeys = users ++ systems;
  "api_dw2_site.age".publicKeys = users ++ systems;

  "discord_classbot.age".publicKeys = users ++ systems;
  "discord_foxy.age".publicKeys = users ++ systems;

  "fo76.age".publicKeys = users ++ systems;

  "valheim.age".publicKeys = users ++ systems;

  "wireguard.age".publicKeys = users ++ systems;
  "acme.age".publicKeys = users ++ systems;

  "mongodb.age".publicKeys = users ++ systems;
  "mongodb_backup.age".publicKeys = users ++ systems;

  # backups
  "restic.age".publicKeys = users ++ systems;
  "backblaze.age".publicKeys = users ++ systems;

  # email stuff
  "email_brendan.age".publicKeys = users ++ systems;
  "email_monica.age".publicKeys = users ++ systems;
  "email_support.age".publicKeys = users ++ systems;
  "email_lemmy.age".publicKeys = users ++ systems;
  "email_lemmy_pw.age".publicKeys = users ++ systems;
  "email_mastodon.age".publicKeys = users ++ systems;
  "email_mastodon_pw.age".publicKeys = users ++ systems;

  # temp for ulfm
  "ulfm.age".publicKeys = users ++ systems;
}
