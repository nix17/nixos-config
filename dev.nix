# run with nix-shell dev.nix
# has everything installed for dev
{pkgs ? import <nixpkgs> {}}:
with pkgs; let
  imports = let
    agenixCommit = "42d371d861a227149dc9a7e03350c9ab8b8ddd68";
  in {
    agenix =
      import
      (builtins.fetchTarball {
        url = "https://github.com/ryantm/agenix/archive/${agenixCommit}.tar.gz";
        sha256 = "14sszf5s85i4jd3lc8c167fbxvpj13da45wl1j7wpd20n0fic5c1";
      })
      {inherit pkgs;};
  };
in
  mkShell {
    # nativeBuildInputs is usually what you want -- tools you need to run
    nativeBuildInputs = [
      #pkgs.buildPackages.git
      #pkgs.buildPackages.colmena
      #pkgs.buildPackages.nmap
    ];

    buildInputs = [imports.agenix.agenix];

    shellHook = ''export EDITOR="${pkgs.nano}/bin/nano"'';
  }
