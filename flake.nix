{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";

    # utility stuff
    home-manager.url = "github:nix-community/home-manager/release-24.05";
    flake-utils.url = "github:numtide/flake-utils";
    agenix.url = "github:ryantm/agenix";
    arion.url = "github:hercules-ci/arion";
    alejandra = {
      url = "github:kamadorueda/alejandra/3.0.0";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Rust stuff
    silver_redirector.url = "gitlab:silver_rust/redirector";
    silver_aoc-badge-api.url = "gitlab:Brendan_Golden-aoc/aoc_badge_api";

    # Personal site
    silver_brendan-ie.url = "gitlab:brendan.ie/v1";

    # JS - Discord bots
    silver_discord_foxy.url = "gitlab:Silvers_General_Stuff/foxy_discord_bot";

    # JS - Utilities
    silver_proxy-rate-limiter.url = "gitlab:Silvers_General_Stuff/proxy-rate-limiter";

    # JS - API
    silver_api_bns.url = "gitlab:Silver_BnS/BnS_Marketplace_Data";
    silver_api_dw2.url = "gitlab:/Silvers_Gw2/Market_Data_Processer";
    #silver_api_dw2.url = "path:/root/js_api_dw2";
    silver_api_dw2_stats.url = "gitlab:Silvers_Gw2/Stats_Backend";
    silver_api_dw2_site.url = "gitlab:Silvers_Gw2/gw2_site_backend";

    # JS - React
    silver_datawars2-frontend.url = "gitlab:Silvers_Gw2/Stats_Frontend";
    silver_fo76 = {
      type = "gitlab";
      owner = "Silvers_General_Stuff%2Ffo76_trading";
      repo = "compose";
    };

    # email
    simple-nixos-mailserver.url = "gitlab:simple-nixos-mailserver/nixos-mailserver/nixos-24.05";

    # uni
    silver_discord_classbot = {
      type = "gitlab";
      owner = "c2842%2Fmisc";
      repo = "classbot";
    };
    silver_better300.url = "gitlab:silver_rust/better-300";
    silver_ul_ical = {
      type = "gitlab";
      owner = "c2842%2Fmisc";
      repo = "ul_timetable_ical";
    };

    compsoc_qr = {
      type = "gitlab";
      owner = "c2842%2Fcomputer_society";
      repo = "ul-healthy-living-qr-code";
    };
    growlr = {
      type = "gitlab";
      owner = "c2842%2Fsemester-6";
      repo = "cs4116-project";
    };
    growlr2 = {
      type = "gitlab";
      owner = "c2842%2Fsemester-8";
      repo = "cs4227_project%2Fpersonal";
    };
  };

  outputs = {
    self,
    nixpkgs,
    agenix,
    alejandra,
    ...
  } @ attrs: let
    system = "x86_64-linux";
    pkgs = nixpkgs.legacyPackages.x86_64-linux.pkgs;
  in {
    formatter.x86_64-linux = alejandra.defaultPackage."x86_64-linux";

    devShells.x86_64-linux.default = pkgs.mkShell {
      name = "Silver build env";
      nativeBuildInputs = [
        pkgs.buildPackages.git
      ];
      buildInputs = [agenix.packages.x86_64-linux.default];
      shellHook = ''export EDITOR="${pkgs.nano}/bin/nano --nonewlines"; unset LD_LIBRARY_PATH;'';
    };

    nixosConfigurations.main-server = nixpkgs.lib.nixosSystem {
      inherit system;
      specialArgs = attrs // {system = system;};
      modules = [
        ./machines/main-server/configuration.nix
        agenix.nixosModules.default
      ];
    };
  };
}
